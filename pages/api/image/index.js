import nc from 'next-connect';
import multer from 'multer';
import path from 'path';
import { z } from 'zod';

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "./images");
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname));
    }
})
const upload = multer({ storage: storage });


const ImageSchema = z.object({
    name: z.string(),
    description: z.string().nullable(),
    image: z.any().nullable(),
    hobby: z.enum(["guitar", "parasol", "dark"]).nullable().default(null)
})

const handler = nc({
    onError: (error, req, res, next) => {
        console.error(error);
        return res.status(500).json({ message: "Something broke" });
    },
    onNoMatch: (req, res) => {
        return res.status(400).json({ message: "Page not found" });
    }
})
    .get((req, res, next) => {
        console.log("FIRST");

        return next();
    }, (req, res) => {
        return res.status(200).json({ message: "GET" });
    })
    .post(upload.single('image'), (req, res) => {

        const data = req.file;
        console.log(data);

        try {

            return res.status(200).json({ message: "POST" });
        } catch (error) {
            console.error(error);
            return res.status(400).json({ error });
        }

    })

export default handler;

export const config = {
    api: {
        bodyParser: false
    }
}